module collectives

  use mpi_f08
  use precisions
  use stats
  use utils
  use, intrinsic :: iso_c_binding
  
  implicit none


contains

  subroutine benchmark(mpi_comm_rows, mpi_comm_cols, np_rows, np_cols, my_rank, num_proc, layout)

    TYPE(MPI_Comm), intent(in)   :: mpi_comm_rows, mpi_comm_cols
    character(len=1), intent(in) :: layout
    integer(kind=i8), intent(in) :: np_cols, np_rows
    integer(kind=i4), intent(in) :: my_rank, num_proc

    ! MPI
    TYPE(c_ptr)   :: win_buffer_c_ptr
    TYPE(MPI_Win) :: win
    integer       :: displacement
    integer(kind=MPI_ADDRESS_KIND)       :: buff_size, lb, size_of_dp, target_disp
    real(kind=dp), pointer, asynchronous :: win_buffer(:)
    integer(kind=i4)                     :: row_rank, col_rank, num_row_proc, num_col_proc

    integer    :: j, itr_counter   ! loop counter
    integer    :: bcast_msg_size, allreduce_msg_size, mpierr
    real(kind=dp), allocatable   :: buffer_bcast(:), buffer_send(:), buffer_recv(:), exec_time(:)
    integer, parameter   ::  matrix_size    = 20000
    integer, parameter   ::  num_iterations = 19935   ! no. iter. ELPA2 does for a matrix of 20,000 size
    integer(kind=i4), parameter    ::  root = 0       ! rank 0 is root

    ! for timing
    real(kind=dp)                   :: t_0, t_1, my_time, min_val, max_val, mean, std 
    integer(kind=MPI_ADDRESS_KIND)  :: synch
    logical                         :: flag


    ! Get info about the new comm groups
    call mpi_comm_size(mpi_comm_rows, num_row_proc)
    call mpi_comm_rank(mpi_comm_rows, row_rank)
    call mpi_comm_size(mpi_comm_cols, num_col_proc)
    call mpi_comm_rank(mpi_comm_cols, col_rank)   

    if(my_rank == root)  then
       allocate(exec_time(num_proc))
       call mpi_comm_get_attr(MPI_COMM_WORLD, MPI_WTIME_IS_GLOBAL, synch, flag, mpierr)
       if (synch /= 1) then
          print *, "Warning: MPI_Wtime() is not synchronized across all ranks."
       else
          print *, "MPI_Wtime() is synchronized across all ranks."
       end if
    end if

  ! Starting the measurements

 !1-> MPI_Barrier() over MPI_COMM_WORLD
    t_0 = mpi_wtime()
       do itr_counter=1, num_iterations
         call mpi_barrier(MPI_COMM_WORLD, mpierr)
       end do 
    t_1 = mpi_wtime()
    my_time = t_1 - t_0

    call mpi_gather(my_time, 1, MPI_DOUBLE_PRECISION, exec_time, 1, MPI_DOUBLE_PRECISION, root, & 
                    MPI_COMM_WORLD)
    if (my_rank == root)  then
      call extract_stats(exec_time, min_val, max_val, mean, std)
      call io("MPI_Barrier over MPI_COMM_WORLD", min_val, max_val, mean, std, num_iterations)
    end if

 !2-> MPI_Barrier() over mpi_comm_rows
    t_0 = mpi_wtime()
       do itr_counter=1, num_iterations
         call mpi_barrier(mpi_comm_rows, mpierr)
       end do 
    t_1 = mpi_wtime()
    my_time = t_1 - t_0

    call mpi_gather(my_time, 1, MPI_DOUBLE_PRECISION, exec_time, 1, MPI_DOUBLE_PRECISION, root, & 
                    MPI_COMM_WORLD)
    if (my_rank == root)  then
      call extract_stats(exec_time, min_val, max_val, mean, std)
      call io("MPI_Barrier over mpi_comm_rows", min_val, max_val, mean, std, num_iterations)
    end if

 !3-> MPI_Barrier() over mpi_comm_cols
    t_0 = mpi_wtime()
       do itr_counter=1, num_iterations
         call mpi_barrier(mpi_comm_cols, mpierr)
       end do 
    t_1 = mpi_wtime()
    my_time = t_1 - t_0

    call mpi_gather(my_time, 1, MPI_DOUBLE_PRECISION, exec_time, 1, MPI_DOUBLE_PRECISION, root, & 
                    MPI_COMM_WORLD)
    if (my_rank == root)  then
      call extract_stats(exec_time, min_val, max_val, mean, std)
      call io("MPI_Barrier over mpi_comm_cols", min_val, max_val, mean, std, num_iterations)
    end if

    bcast_msg_size = matrix_size/np_rows
    allreduce_msg_size = np_cols/2

    if (my_rank == root)  then
        print *, 'Bcast message size: ', bcast_msg_size
        print *, 'Allreduce message size: ', allreduce_msg_size
    end if

    allocate(buffer_bcast(bcast_msg_size))
    allocate(buffer_send(allreduce_msg_size))    
    allocate(buffer_recv(allreduce_msg_size))    

    ! initialize the buffers
    buffer_bcast(:) = 1._dp  ;  buffer_send(:)  = 1._dp  ;  buffer_recv(:)  = 1._dp

 !4-> MPI_Bcast() over mpi_comm_cols
    t_0 = mpi_wtime()
       do itr_counter=1, num_iterations
          call mpi_bcast(buffer_bcast, bcast_msg_size, MPI_DOUBLE_PRECISION, 0, mpi_comm_cols, mpierr)
       end do    
    t_1 = mpi_wtime()
    my_time = t_1 - t_0

    call mpi_gather(my_time, 1, MPI_DOUBLE_PRECISION, exec_time, 1, MPI_DOUBLE_PRECISION, root, & 
                    MPI_COMM_WORLD)
    if (my_rank == root)  then
      call extract_stats(exec_time, min_val, max_val, mean, std)
      call io("MPI_Bcast over mpi_comm_cols", min_val, max_val, mean, std, num_iterations)
    end if

 !5-> MPI_Bcast() over mpi_comm_rows
    t_0 = mpi_wtime()
       do itr_counter=1, num_iterations
          call mpi_bcast(buffer_bcast, bcast_msg_size, MPI_DOUBLE_PRECISION, 0, mpi_comm_rows, mpierr)
       end do 
    t_1 = mpi_wtime()
    my_time = t_1 - t_0

    call mpi_gather(my_time, 1, MPI_DOUBLE_PRECISION, exec_time, 1, MPI_DOUBLE_PRECISION, root, & 
                    MPI_COMM_WORLD)
    if (my_rank == root)  then
      call extract_stats(exec_time, min_val, max_val, mean, std)
      call io("MPI_Bcast over mpi_comm_rows", min_val, max_val, mean, std, num_iterations)
    end if

 !6-> Bcast() via one-sided comm over mpi_comm_cols
    call mpi_type_get_extent(MPI_DOUBLE_PRECISION, lb, size_of_dp, mpierr)
    displacement = size_of_dp
    buff_size = bcast_msg_size*displacement
    target_disp = 0

    call MPI_Win_allocate(buff_size, displacement, MPI_INFO_NULL, mpi_comm_cols, win_buffer_c_ptr, win)
    call c_f_pointer(win_buffer_c_ptr, win_buffer, (/bcast_msg_size/))

    t_0 = mpi_wtime()
       do itr_counter=1, num_iterations
          call mpi_win_fence(MPI_MODE_NOSTORE + MPI_MODE_NOPRECEDE, win)
          if (col_rank == 0) then
             do j=1,num_col_proc-1
                call MPI_Put(buffer_bcast, bcast_msg_size, MPI_DOUBLE_PRECISION, j, & 
                             target_disp, bcast_msg_size, MPI_DOUBLE_PRECISION, win)
             end do
          end if
          call mpi_win_fence(MPI_MODE_NOSTORE + MPI_MODE_NOPUT + MPI_MODE_NOSUCCEED, win)
       end do
    t_1 = mpi_wtime()
    my_time = t_1 - t_0
    call mpi_win_free(win, mpierr)

    call mpi_gather(my_time, 1, MPI_DOUBLE_PRECISION, exec_time, 1, MPI_DOUBLE_PRECISION, root, & 
                    MPI_COMM_WORLD)
    if (my_rank == root)  then
      call extract_stats(exec_time, min_val, max_val, mean, std)
      call io("One-sided comm. over mpi_comm_cols", min_val, max_val, mean, std, num_iterations)
    end if

 !7-> Bcast() via one-sided comm over mpi_comm_rows
    call MPI_Win_allocate(buff_size, displacement, MPI_INFO_NULL, mpi_comm_rows, win_buffer_c_ptr, win)
    call c_f_pointer(win_buffer_c_ptr, win_buffer, (/bcast_msg_size/))

    t_0 = mpi_wtime()
       do itr_counter=1, num_iterations
          call mpi_win_fence(MPI_MODE_NOSTORE + MPI_MODE_NOPRECEDE, win)
          if (row_rank == 0) then
             do j=1,num_row_proc-1
                call MPI_Put(buffer_bcast, bcast_msg_size, MPI_DOUBLE_PRECISION, j, & 
                             target_disp, bcast_msg_size, MPI_DOUBLE_PRECISION, win)
             end do
          end if
          call mpi_win_fence(MPI_MODE_NOSTORE + MPI_MODE_NOPUT + MPI_MODE_NOSUCCEED, win)
       end do
    t_1 = mpi_wtime()
    my_time = t_1 - t_0
    call mpi_win_free(win, mpierr)

    call mpi_gather(my_time, 1, MPI_DOUBLE_PRECISION, exec_time, 1, MPI_DOUBLE_PRECISION, root, & 
                    MPI_COMM_WORLD)
    if (my_rank == root)  then
      call extract_stats(exec_time, min_val, max_val, mean, std)
      call io("One-sided comm. over mpi_comm_rows", min_val, max_val, mean, std, num_iterations)
    end if

 !8-> MPI_Allreduce() over mpi_comm_cols
    t_0 = mpi_wtime()
       do itr_counter=1, num_iterations
           call mpi_allreduce(buffer_send, buffer_recv, allreduce_msg_size, MPI_DOUBLE_PRECISION, & 
                              MPI_SUM, mpi_comm_cols, mpierr)
       end do
    t_1 = mpi_wtime()
    my_time = t_1 - t_0

    call mpi_gather(my_time, 1, MPI_DOUBLE_PRECISION, exec_time, 1, MPI_DOUBLE_PRECISION, root, & 
                    MPI_COMM_WORLD)
    if (my_rank == root)  then
      call extract_stats(exec_time, min_val, max_val, mean, std)
      call io("MPI_Allreduce over mpi_comm_cols", min_val, max_val, mean, std, num_iterations)
    end if

 !9-> MPI_Allreduce() over mpi_comm_rows
    t_0 = mpi_wtime()
       do itr_counter=1, num_iterations
           call mpi_allreduce(buffer_send, buffer_recv, allreduce_msg_size, MPI_DOUBLE_PRECISION, & 
                              MPI_SUM, mpi_comm_rows, mpierr)
       end do 
    t_1 = mpi_wtime()
    my_time = t_1 - t_0

    call mpi_gather(my_time, 1, MPI_DOUBLE_PRECISION, exec_time, 1, MPI_DOUBLE_PRECISION, root, & 
                    MPI_COMM_WORLD)
    if (my_rank == root)  then
      call extract_stats(exec_time, min_val, max_val, mean, std)
      call io("MPI_Allreduce over mpi_comm_rows", min_val, max_val, mean, std, num_iterations)
    end if

    if (allocated(buffer_bcast)) deallocate(buffer_bcast)
    if (allocated(buffer_send)) deallocate(buffer_send)
    if (allocated(buffer_recv)) deallocate(buffer_recv)
    
    ! end of benchmark
 
  end subroutine benchmark

end module collectives

